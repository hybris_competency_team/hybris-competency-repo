/**
 *
 */
package com.sonata.distributor.facades.populators;

import de.hybris.platform.acceleratorcms.model.components.CategoryFeatureComponentModel;
import de.hybris.platform.commerceservices.converter.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.sonata.distributor.facades.product.data.CategoryFeatureComponentData;


/**
 * @author nithin.kv
 *
 */
public class CategoryFeatureComponentPopulator implements Populator<CategoryFeatureComponentModel, CategoryFeatureComponentData>
{

	@Override
	public void populate(final CategoryFeatureComponentModel source, final CategoryFeatureComponentData target)
			throws ConversionException
	{
		target.setDescription(source.getDescription());
		target.setTitle(source.getTitle());
		if (source.getCategory().getThumbnail() != null)
		{
			target.setMedia(source.getCategory().getThumbnail().getUrl());
		}
		target.setCategoryCode(source.getCategory().getCode());
	}

}
