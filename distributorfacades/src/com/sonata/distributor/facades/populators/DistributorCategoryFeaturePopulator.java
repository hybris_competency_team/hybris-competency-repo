/**
 *
 */
package com.sonata.distributor.facades.populators;

import de.hybris.platform.acceleratorcms.model.components.CategoryFeatureComponentModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.converters.populator.CategoryPopulator;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import com.sonata.distributor.facades.product.data.CategoryFeatureComponentData;


/**
 * @author nithin.kv
 *
 */
public class DistributorCategoryFeaturePopulator extends CategoryPopulator
{

	private Converter<CategoryFeatureComponentModel, CategoryFeatureComponentData> categoryFeatureComponentConverter;

	@Override
	public void populate(final CategoryModel source, final CategoryData target)
	{
		super.populate(source, target);
		if (source.getCategoryFeatureComponents() != null)
		{
			for (final CategoryFeatureComponentModel categoryFeatureComponentModel : source.getCategoryFeatureComponents())
			{
				target.setCategoryFeatureComponent(getCategoryFeatureComponentConverter().convert(categoryFeatureComponentModel));
			}
		}
	}

	public Converter<CategoryFeatureComponentModel, CategoryFeatureComponentData> getCategoryFeatureComponentConverter()
	{
		return categoryFeatureComponentConverter;
	}

	public void setCategoryFeatureComponentConverter(
			final Converter<CategoryFeatureComponentModel, CategoryFeatureComponentData> categoryFeatureComponentConverter)
	{
		this.categoryFeatureComponentConverter = categoryFeatureComponentConverter;
	}


}
