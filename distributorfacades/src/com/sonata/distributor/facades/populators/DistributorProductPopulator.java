/**
 *
 */
package com.sonata.distributor.facades.populators;


import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.math.BigDecimal;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Required;

import com.sonata.distributor.core.model.DistributorProductModel;
import com.sonata.distributor.facades.data.DistributorProductData;



/**
 * @author sahana.m
 *
 */
public class DistributorProductPopulator implements Populator<DistributorProductModel, DistributorProductData>
{

	private PriceDataFactory priceDataFactory;
	private Converter<ProductModel, ProductData> productConverter;
	private Converter<ProductModel, ProductData> productStockConverter;
	private Converter<ProductModel, ProductData> priceProductConverter;


	@SuppressWarnings("deprecation")
	@Override
	public void populate(final DistributorProductModel source, final DistributorProductData target) throws ConversionException
	{
		target.setMinOrderQuantity(source.getMinOrderQuantity());
		target.setMaxOrderQuantity(source.getMaxOrderQuantity());
		target.setPrice(createPrice(source.getEurope1Prices()));
		getProductConverter().convert(source, target);
		target.setNorm(source.getNorm());
		target.setAverageSales(source.getAverageSales());
		target.setInTransit(source.getInTransit());
		target.setCoverDays(source.getCoverDays());
		if (source.getProposedQty() != null)
		{
			target.setProposedQty(source.getProposedQty());
		}
		target.setOpenQty(source.getOpenQty());
		target.setChangedProposedQty(source.getChangedProposedQty());
		getProductStockConverter().convert(source, target);


	}

	protected PriceData createPrice(final Collection<PriceRowModel> priceRowModelList)
	{
		PriceData priceData = new PriceData();
		for (final PriceRowModel priceRowModel : priceRowModelList)
		{
			priceData = getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(priceRowModel.getPrice().doubleValue()),
					priceRowModel.getCurrency());
			break;
		}
		return priceData;
	}






	protected PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	@Required
	public void setPriceDataFactory(final PriceDataFactory priceDataFactory)
	{
		this.priceDataFactory = priceDataFactory;
	}

	/**
	 * @return the stockConverter
	 */
	/**
	 * @return the productConverter
	 */
	public Converter<ProductModel, ProductData> getProductConverter()
	{
		return productConverter;
	}

	/**
	 * @param productConverter
	 *           the productConverter to set
	 */
	public void setProductConverter(final Converter<ProductModel, ProductData> productConverter)
	{
		this.productConverter = productConverter;
	}


	/**
	 * @return the productStockConverter
	 */
	public Converter<ProductModel, ProductData> getProductStockConverter()
	{
		return productStockConverter;
	}

	/**
	 * @param productStockConverter
	 *           the productStockConverter to set
	 */
	public void setProductStockConverter(final Converter<ProductModel, ProductData> productStockConverter)
	{
		this.productStockConverter = productStockConverter;
	}

	/**
	 * @return the priceProductConverter
	 */
	public Converter<ProductModel, ProductData> getPriceProductConverter()
	{
		return priceProductConverter;
	}

	/**
	 * @param priceProductConverter
	 *           the priceProductConverter to set
	 */
	public void setPriceProductConverter(final Converter<ProductModel, ProductData> priceProductConverter)
	{
		this.priceProductConverter = priceProductConverter;
	}




}
