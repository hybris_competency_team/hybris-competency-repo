/**
 *
 */
package com.sonata.distributor.facades;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;

import java.util.List;
import java.util.Map;

import com.sonata.distributor.facades.data.DistributorProductData;
import com.sonata.distributor.facades.product.data.CategoryFeatureComponentData;


/**
 * @author nithin.kv
 *
 */
public interface DistributorFacade
{

	public List<CategoryFeatureComponentData> getCategoryFeatureComponent(final String categoryCode);

	public List<CategoryFeatureComponentData> getAllCatgoryFeatureComponents();

	public List<OrderEntryData> getProductDetails(String categoryCode);

	public DistributorProductData getProduct(String productCode);

	public Map<String, String> getDistributorDetails();

}
