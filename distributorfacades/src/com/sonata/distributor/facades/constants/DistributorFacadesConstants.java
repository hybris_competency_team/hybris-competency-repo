/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.sonata.distributor.facades.constants;

/**
 * Global class for all DistributorFacades constants.
 */
@SuppressWarnings("PMD")
public class DistributorFacadesConstants extends GeneratedDistributorFacadesConstants
{
	public static final String EXTENSIONNAME = "distributorfacades";

	private DistributorFacadesConstants()
	{
		//empty
	}
}
