/**
 *
 */
package com.sonata.distributor.facades.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.b2bacceleratorfacades.company.data.UserData;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.sonata.distributor.core.model.DistributorProductModel;
import com.sonata.distributor.core.services.DistributorService;
import com.sonata.distributor.facades.DistributorFacade;
import com.sonata.distributor.facades.data.DistributorProductData;
import com.sonata.distributor.facades.product.data.CategoryFeatureComponentData;


/**
 * @author nithin.kv
 *
 */
public class DefaultDistributorFacade implements DistributorFacade
{

	private Converter<DistributorProductModel, DistributorProductData> distributorProductConverter;

	private DistributorService distributorService;
	private CategoryService categoryService;
	private B2BUnitService<B2BUnitModel, UserModel> b2bUnitService;
	private Converter<CategoryModel, CategoryData> categoryConverter;
	private Converter<B2BCustomerModel, UserData> distributorConverter;

	@Resource(name = "userService")
	private UserService userService;

	private String rootCategory;

	@Override
	public List<CategoryFeatureComponentData> getCategoryFeatureComponent(final String categoryCode)
	{

		final CategoryModel category = getCategoryService().getCategoryForCode(categoryCode);
		final Collection<CategoryModel> subCategoriesModelList = category.getAllSubcategories();
		final List<CategoryFeatureComponentData> categoryFeatureComponentDataList = new ArrayList<CategoryFeatureComponentData>();
		final List<String> categoriesForB2bCustomer = new ArrayList<String>();

		if ((getUserService().getCurrentUser()) instanceof B2BCustomerModel)
		{
			final B2BCustomerModel b2bcustomer = (B2BCustomerModel) getUserService().getCurrentUser();
			if (b2bcustomer.getCategory() != null)
			{
				for (final CategoryModel b2bcategory : b2bcustomer.getCategory())
				{
					categoriesForB2bCustomer.add(b2bcategory.getCode());
				}
			}
		}

		for (final CategoryModel subCategory : subCategoriesModelList)
		{
			if (categoriesForB2bCustomer.contains(subCategory.getCode()))
			{
				final CategoryData subCategoryData = categoryConverter.convert(subCategory);
				categoryFeatureComponentDataList.add(subCategoryData.getCategoryFeatureComponent());
			}
		}

		return categoryFeatureComponentDataList;
	}

	@Override
	public List<CategoryFeatureComponentData> getAllCatgoryFeatureComponents()
	{

		final String rootCategory = getRootCategory();
		final List<CategoryFeatureComponentData> categoryFeatureComponentDataList = getCategoryFeatureComponent(rootCategory);

		return categoryFeatureComponentDataList;
	}

	@Override
	public List<OrderEntryData> getProductDetails(final String categoryCode)
	{


		final List<OrderEntryData> orderEntryDataList = new ArrayList<OrderEntryData>();

		for (final DistributorProductModel distributorProductModel : getDistributorService().getProdutsDetails(categoryCode,
				userService.getCurrentUser().getUid()))
		{
			final OrderEntryData orderEntryData = new OrderEntryData();
			orderEntryData.setDistributorProductData(distributorProductConverter.convert(distributorProductModel));
			final Long obj = new Long(1);
			orderEntryData.setQuantity(obj);
			orderEntryDataList.add(orderEntryData);
		}
		return orderEntryDataList;
	}

	@Override
	public DistributorProductData getProduct(final String productCode)
	{
		DistributorProductData distributorProductData = new DistributorProductData();
		for (final DistributorProductModel distributorProductModel : getDistributorService().getProduct(productCode))
		{
			distributorProductData = distributorProductConverter.convert(distributorProductModel);
			break;
		}
		return distributorProductData;
	}

	/**
	 * @return the b2bUnitService
	 */
	public B2BUnitService<B2BUnitModel, UserModel> getB2bUnitService()
	{
		return b2bUnitService;
	}

	/**
	 * @param b2bUnitService
	 *           the b2bUnitService to set
	 */
	public void setB2bUnitService(final B2BUnitService<B2BUnitModel, UserModel> b2bUnitService)
	{
		this.b2bUnitService = b2bUnitService;
	}


	public CategoryService getCategoryService()
	{
		return categoryService;
	}

	public void setCategoryService(final CategoryService categoryService)
	{
		this.categoryService = categoryService;
	}

	public Converter<CategoryModel, CategoryData> getCategoryConverter()
	{
		return categoryConverter;
	}

	public void setCategoryConverter(final Converter<CategoryModel, CategoryData> categoryConverter)
	{
		this.categoryConverter = categoryConverter;
	}

	public UserService getUserService()
	{
		return userService;
	}

	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public String getRootCategory()
	{
		return rootCategory;
	}

	public void setRootCategory(final String rootCategory)
	{
		this.rootCategory = rootCategory;
	}

	/**
	 * @return the distributorProductConverter
	 */
	public Converter<DistributorProductModel, DistributorProductData> getDistributorProductConverter()
	{
		return distributorProductConverter;
	}

	/**
	 * @param distributorProductConverter
	 *           the distributorProductConverter to set
	 */
	public void setDistributorProductConverter(
			final Converter<DistributorProductModel, DistributorProductData> distributorProductConverter)
	{
		this.distributorProductConverter = distributorProductConverter;
	}

	/**
	 * @return the distributorService
	 */
	public DistributorService getDistributorService()
	{
		return distributorService;
	}

	/**
	 * @param distributorService
	 *           the distributorService to set
	 */
	public void setDistributorService(final DistributorService distributorService)
	{
		this.distributorService = distributorService;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.sonata.distributor.facades.DistributorFacade#getDistributorDetails()
	 */
	@Override
	public Map<String, String> getDistributorDetails()
	{
		// YTODO Auto-generated method stub
		final B2BCustomerModel b2bCustomerModel = (B2BCustomerModel) userService.getCurrentUser();
		final B2BUnitModel parent = getB2bUnitService().getParent(b2bCustomerModel);

		final String uid = getUserService().getCurrentUser().getUid();
		final String name = getUserService().getCurrentUser().getName();
		final Map<String, String> distributorDetails = new HashMap<String, String>();
		distributorDetails.put("uid", uid);
		distributorDetails.put("name", name);
		distributorDetails.put("unitCode", parent.getUid());
		return distributorDetails;

	}

	/**
	 * @return the distributorConverter
	 */
	public Converter<B2BCustomerModel, UserData> getDistributorConverter()
	{
		return distributorConverter;
	}

	/**
	 * @param distributorConverter
	 *           the distributorConverter to set
	 */
	public void setDistributorConverter(final Converter<B2BCustomerModel, UserData> distributorConverter)
	{
		this.distributorConverter = distributorConverter;
	}


}
