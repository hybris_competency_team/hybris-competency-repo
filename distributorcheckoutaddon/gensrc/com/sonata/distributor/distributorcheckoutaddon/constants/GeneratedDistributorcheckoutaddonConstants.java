/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Oct 12, 2016 1:05:41 PM                     ---
 * ----------------------------------------------------------------
 */
package com.sonata.distributor.distributorcheckoutaddon.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedDistributorcheckoutaddonConstants
{
	public static final String EXTENSIONNAME = "distributorcheckoutaddon";
	
	protected GeneratedDistributorcheckoutaddonConstants()
	{
		// private constructor
	}
	
	
}
