package com.sonata.distributor.core.jalo;

import com.sonata.distributor.core.constants.DistributorCoreConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

@SuppressWarnings("PMD")
public class DistributorCoreManager extends GeneratedDistributorCoreManager
{
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger( DistributorCoreManager.class.getName() );
	
	public static final DistributorCoreManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (DistributorCoreManager) em.getExtension(DistributorCoreConstants.EXTENSIONNAME);
	}
	
}
