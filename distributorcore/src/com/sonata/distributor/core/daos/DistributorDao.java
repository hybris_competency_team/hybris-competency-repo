/**
 *
 */
package com.sonata.distributor.core.daos;

import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.List;

import com.sonata.distributor.core.model.DistributorProductModel;


/**
 * @author sahana.m
 *
 */
public interface DistributorDao extends Dao
{
	public List<DistributorProductModel> getProdutsDetails(String categoryCode, String customerUid);

	public List<DistributorProductModel> getProduct(String productCode);

	//public void setOrderConfirmationDetails(DistributorOrderModel distributorOrderModel);

	//public List<DistributorOrderModel> getOrderConfirmationDetails();
}
