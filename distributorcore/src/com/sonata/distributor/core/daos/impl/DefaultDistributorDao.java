/**
 *
 */
package com.sonata.distributor.core.daos.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.link.LinkModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sonata.distributor.core.daos.DistributorDao;
import com.sonata.distributor.core.model.DistributorProductModel;


/**
 * @author sahana.m
 *
 */
public class DefaultDistributorDao extends AbstractItemDao implements DistributorDao
{




	@Override
	public List<DistributorProductModel> getProdutsDetails(final String categoryCode, final String customerUid)
	{
		final Map params = new HashMap();
		final StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT {p:").append(DistributorProductModel.PK).append("} ");
		queryString.append("FROM {").append(DistributorProductModel._TYPECODE).append(" AS p ");
		queryString.append("JOIN ").append(DistributorProductModel._CATEGORYPRODUCTRELATION).append(" AS l ");
		queryString.append("ON {l:").append(LinkModel.TARGET).append("}={p:").append(DistributorProductModel.PK);
		queryString.append("}JOIN ").append(B2BCustomerModel._DISTRIBUTORPRODUCT2CUSTOMER).append(" AS dc ");
		queryString.append("ON {dc:").append(LinkModel.SOURCE).append("}={p:").append(DistributorProductModel.PK).append("} } ");
		queryString.append("WHERE ").append("{l:").append(LinkModel.SOURCE).append("}");
		queryString.append(" IN ( {{ SELECT { cat." + CategoryModel.PK + "} FROM {" + CategoryModel._TYPECODE + " AS cat}");
		queryString.append("WHERE { cat. " + CategoryModel.CODE + "} =(?" + DistributorProductModel.CODE + ")}})");
		queryString.append("AND ").append("{dc:").append(LinkModel.TARGET).append("}");
		queryString.append(" IN ( {{ SELECT { b2bcus." + B2BCustomerModel.PK + "} FROM {" + B2BCustomerModel._TYPECODE
				+ " AS b2bcus}");
		queryString.append("WHERE { b2bcus. " + B2BCustomerModel.UID + "} =(?" + B2BCustomerModel.UID + ")}})");


		params.put(DistributorProductModel.CODE, categoryCode);
		params.put(B2BCustomerModel.UID, customerUid);

		final SearchResult<DistributorProductModel> searchRes = search(queryString.toString(), params);
		return searchRes.getResult();
	}


	@Override
	public List<DistributorProductModel> getProduct(final String productCode)
	{
		final Map params = new HashMap();
		final StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT {p.").append(DistributorProductModel.PK).append("} ");
		queryString.append("FROM {").append(DistributorProductModel._TYPECODE).append(" AS p} ");
		queryString.append("WHERE { p. " + DistributorProductModel.CODE + "} =(?" + DistributorProductModel.CODE + ")");


		params.put(DistributorProductModel.CODE, productCode);

		final SearchResult<DistributorProductModel> searchRes = search(queryString.toString(), params);
		return searchRes.getResult();
	}


}
