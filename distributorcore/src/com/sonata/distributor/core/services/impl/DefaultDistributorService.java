/**
 *
 */
package com.sonata.distributor.core.services.impl;

import java.util.List;

import javax.annotation.Resource;

import com.sonata.distributor.core.daos.DistributorDao;
import com.sonata.distributor.core.model.DistributorProductModel;
import com.sonata.distributor.core.services.DistributorService;


/**
 * @author sahana.m
 *
 */
public class DefaultDistributorService implements DistributorService
{
	@Resource(name = "distributorDao")
	DistributorDao distributorDao;


	@Override
	public List<DistributorProductModel> getProdutsDetails(final String categoryCode, final String userUid)
	{
		/*
		 * final List<DistributorProductModel> distributorProductModelList = new ArrayList<DistributorProductModel>(); for
		 * (final DistributorProductModel distributorProductModel : distributorDao.getProdutsDetails(categoryCode)) { for
		 * (final B2BCustomerModel b2bCustomerModel : distributorProductModel.getDistributor()) {
		 * b2bCustomerModel.getUid().equalsIgnoreCase(userUid); distributorProductModelList.add(distributorProductModel);
		 * } }
		 */

		return distributorDao.getProdutsDetails(categoryCode, userUid);
	}

	@Override
	public List<DistributorProductModel> getProduct(final String productCode)
	{
		return distributorDao.getProduct(productCode);
	}


	/**
	 * @return the distributorDao
	 */
	public DistributorDao getDistributorDao()
	{
		return distributorDao;
	}

	/**
	 * @param distributorDao
	 *           the distributorDao to set
	 */
	public void setDistributorDao(final DistributorDao distributorDao)
	{
		this.distributorDao = distributorDao;
	}










}
