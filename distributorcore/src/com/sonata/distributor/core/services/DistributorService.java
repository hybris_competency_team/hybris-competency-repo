/**
 *
 */
package com.sonata.distributor.core.services;

import java.util.List;

import com.sonata.distributor.core.model.DistributorProductModel;


/**
 * @author sahana.m
 *
 */
public interface DistributorService
{
	public List<DistributorProductModel> getProdutsDetails(String categoryCode, String userUid);

	public List<DistributorProductModel> getProduct(String productCode);
}
