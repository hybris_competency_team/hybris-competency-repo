ACC.common = {
	currentCurrency: "USD",
	$page: $("#page"),

	setCurrentCurrency: function ()
	{
		ACC.common.currentCurrency = ACC.common.$page.data("currencyIsoCode");
	},

	refreshScreenReaderBuffer: function ()
	{
		// changes a value in a hidden form field in order
		// to trigger a buffer update in a screen reader
		$('#accesibility_refreshScreenReaderBufferField').attr('value', new Date().getTime());
	},

	bindAll: function ()
	{
		ACC.common.bindToUiCarouselLink();
		ACC.common.bindShowProcessingMessageToSubmitButton();
	},

	bindToUiCarouselLink: function ()
	{
		$("ul.carousel > li a.popup").colorbox({
			onComplete: function ()
			{
				ACC.common.refreshScreenReaderBuffer();
				$.colorbox.resize()
				ACC.product.initQuickviewLightbox();
			},

			onClosed: function ()
			{
				ACC.common.refreshScreenReaderBuffer();
			}
		});
	},

	processingMessage: $("<img src='" + ACC.config.commonResourcePath + "/images/spinner.gif'/>"),

	bindShowProcessingMessageToSubmitButton: function ()
	{

		$(':submit.show_processing_message').each(function ()
		{
			$(this).on("click", ACC.common.showProcessingMessageAndBlockForm)
		});
	},

	showProcessingMessageAndBlockForm: function ()
	{
		$("#checkoutContentPanel").block({ message: ACC.common.processingMessage });
	},

	blockFormAndShowProcessingMessage: function (submitButton)
	{
		var form = submitButton.parents('form:first');
		form.block({ message: ACC.common.processingMessage });
	},

	showSpinnerById: function(id) {
		$('#'+id).show();
	},

	hideSpinnerById: function(id) {
		$('#'+id).hide();
	}
};

$(document).ready(function ()
{
	ACC.common.setCurrentCurrency();
	ACC.common.bindAll();
});


/* Extend jquery with a postJSON method */
jQuery.extend({
	postJSON: function (url, data, callback)
	{
		return jQuery.post(url, data, callback, "json");
	}
});

// add a CSRF request token to POST ajax request if its not available
$.ajaxPrefilter(function (options, originalOptions, jqXHR)
{
	// Modify options, control originalOptions, store jqXHR, etc
	if (options.type === "post" || options.type === "POST")
	{
		var noData = (typeof options.data === "undefined");
		if (noData || options.data.indexOf("CSRFToken") === -1)
		{
			options.data = (!noData ? options.data + "&" : "") + "CSRFToken=" + ACC.config.CSRFToken;
		}
	}
});

//distributor order entry total grandtotal & validations
var quantityId;
var priceVal;
var maxValue;
var minValue;
var price_qntArray=[];

$(document).ready(function(){
	var gtotal=0.0;
	var gquantity=0;
	   $("#s tbody td").each(function(){ 
		  $(this).find("input.orderEntry:hidden").each(function(){
			  var pq_lp=$(this).val();
			  var priceQuant=pq_lp.split("_");
			  price_qntArray.push(priceQuant);	 
		  });
	   });
		   $(this).find("#s tbody td input.rectangle:text").each(function(index){
			  // alert(index);
			   var inId=$(this).attr("id");
				   $('#'+inId).val(Number(price_qntArray[index][0].replace(/[^0-9\.]+/g,"")));
				   var listPrice = Number(price_qntArray[index][1].replace(/[^0-9\.]+/g,""));
				  // alert(price_qntArray[i][0]);
				   var result=price_qntArray[index][0]*listPrice;
				   var grandQuantity=Number(price_qntArray[index][0].replace(/[^0-9\.]+/g,""));
				   $('#'+price_qntArray[index][2]).html(result);
				   gtotal=gtotal+result;
				   gquantity=gquantity+grandQuantity;
		    });
	   $('#grandTotal').val(gtotal);
	   $('#grandQuntity').val(gquantity);
	  
  
});  

function reloadPriceQuantity(){
	var gquantity=0;
	var gtotal=0.0;
	$("#s tbody td").each(function(){
		   $(this).find("input.rectangle:text").each(function(){
			   var inId=$(this).attr("id");
			   gquantity=gquantity+Number($('#'+inId).val().replace(/[^0-9\.]+/g,""));
			   var tot=inId.split("_");
			  gtotal=gtotal+Number($('#'+tot[0]).html().replace(/[^0-9\.]+/g,""));
		    });
	   });
	   $('#grandTotal').val(gtotal);
	   $('#grandQuntity').val(gquantity);
}

function checkOrderQuantity(){
	/*alert("quantityvalue is "+$("#"+quantityId).val()+"maxvalue----"+maxValue+"--minvalue is s"+minValue);*/
	if(Number($('#grandTotal').val().replace(/[^0-9\.]+/g,""))<=0){
		return false;
	}
	 if($("#"+quantityId).val()!=0 &&($("#"+quantityId).val()>maxValue || $("#"+quantityId).val()<minValue)){
		 return false;
	 }else{
		 $("#orderForm").submit();
	 }
}
function calculateValue(code, price, pq, mq){
	
	var res=Number(pq.replace(/[^0-9\.]+/g,""));
	var res1=Number(mq.replace(/[^0-9\.]+/g,""));
	var tol=(res/100)*res1;
	var max=res+tol;
	
	var min=res-tol;
	maxValue=max;
	minValue=min;
		
	quantityId=code;
	priceVal=price;
	var tot=quantityId.split("_");
	var number = Number(price.replace(/[^0-9\.]+/g,""));
	var result=parseFloat(number).toFixed(2);
	var userQnt=Number($('#'+quantityId).val().replace(/[^0-9\.]+/g,""));

	if(userQnt==0 && $('#'+quantityId).val()!=""){
	
		loadPage(quantityId,parseInt(userQnt),max,min,tot,result);
	}else{
		if(userQnt!=0 && $.isNumeric($('#'+quantityId).val())){
			
			loadPage(quantityId,parseInt(userQnt),max,min,tot,result);
		}else{
			alert("Please enter valid numeric value");
			$('#'+quantityId).val('0');
			$('#'+tot[0]).html(result*parseInt($('#'+quantityId).val()));
			reloadPriceQuantity();
			$('#'+quantityId).css("background","white");
		}
	}
}

function loadPage(quantityId,userQnt,max,min,tot,result){
	var bool=$('#'+quantityId).val()<0;
	if(bool){
		alert("Please enter positive values.....!");
		$('#'+quantityId).css("background","red");
		
	}else{
		
		$('#'+quantityId).css("background","white");
	
		if(userQnt!=0 &&(userQnt>Math.round(max) || userQnt<Math.round(min))){
			if(Math.round(maxValue)==Math.round(minValue)){
				alert("Qty should be "+Math.round(maxValue)+" based on Qty tolerance value assigned");
			}else{
				alert("Qty should be between "+Math.round(min)+"-"+Math.round(max) +" based on Qty tolerance value assigned");
			}
			
			
			$('#'+quantityId).css("background","red");
			
			$('#'+tot[0]).html("Total");
			
		}else{
			$('#'+quantityId).val(parseInt(userQnt));
			$('#'+tot[0]).html(result*parseInt($('#'+quantityId).val()));
			reloadPriceQuantity();
			$('#'+quantityId).css("background","white");
		}
	}
}
