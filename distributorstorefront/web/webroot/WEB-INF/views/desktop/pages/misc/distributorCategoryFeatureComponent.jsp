<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

 <%-- <div class="prod_cat">
<table>
	<tr>
		<c:forEach items="${categoryFeatureComponentList}" var="categoryFeatureComponent">
			 <td>
				 <div class="title">
					<h2>${categoryFeatureComponent.title}</h2>
					<a href="category/${categoryFeatureComponent.categoryCode}"><img title="${categoryFeatureComponent.title}" alt="${categoryFeatureComponent.title}" src="${categoryFeatureComponent.media}"></a>
					<a href="category/${categoryFeatureComponent.categoryCode}">${categoryFeatureComponent.categoryCode}</a>
					<theme:image code="img.iconArrowCategoryTile" alt="${categoryFeatureComponent.title}"/>
				</div>
				<div class="action">
					<theme:image code="img.iconArrowCategoryTile" alt="${categoryFeatureComponent.title}"/>
				</div>
			</td>
		</c:forEach>
	</tr>
</table>	
</div> --%>

<div class="prod_cat">
	<table>
		<c:forEach items="${categoryFeatureComponentList}" var="categoryFeatureComponent">
			<c:set var="i" scope="session" value="${1}"/>
			<c:choose>
				<c:when test="${i <= 4}">
					<td style="padding-right:18px;">
						<div class="title">
							<h2>${categoryFeatureComponent.title}</h2>
							<c:url var="carturl" value="/cart/" />
							<a href="${carturl}${categoryFeatureComponent.categoryCode}"><img  style="border: 10;" title="${categoryFeatureComponent.title}" alt="${categoryFeatureComponent.title}" src="${categoryFeatureComponent.media}"></a>
							
						</div>										
					</td>
					<c:set var="i" value="${i + 1}"/>
				</c:when>
				<c:otherwise>
						<tr/><td style="padding-right:18px;"><div class="title">
							<h2>${categoryFeatureComponent.title}</h2>
							<c:url var="carturl" value="/cart/" />
							<a href="${carturl}${categoryFeatureComponent.categoryCode}"><img title="${categoryFeatureComponent.title}" alt="${categoryFeatureComponent.title}" src="${categoryFeatureComponent.media}"></a>
							
						</div>	</td>
					<c:remove var="i"/>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</table>	
</div>