<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<style>
table, th, td {
    border: 1px solid black;
}
th,td { 
text-align:center;
}
 input {
    width: 30%;
} 
confirmQty{

	width:1px;
}
</style>

<h3><b><spring:theme code="distributor.order.confirmation"/>${orderCode}</b></h3>

<div align="center">
        <table border="2" cellpadding="5">
        <tbody>
            <tr>
            <!--  <th>Slno</th> -->
                <th class="product"><spring:theme code="distributor.order.form.product.code"/></th>
                <th class="material"><spring:theme code="distributor.order.form.material.description"/></th>
                <th class="norm"><spring:theme code="distributor.order.form.norm"/></th>
                 <th class="sales"><spring:theme code="distributor.order.form.average.sales"/></th>
                 <th class="transit"><spring:theme code="distributor.order.form.in.transit"/></th>
                 <th class="days"><spring:theme code="distributor.order.form.cover.days"/></th>
                <th class="stock"><spring:theme code="distributor.order.form.stock"/></th>
                <th class="quantity"><spring:theme code="distributor.order.form.propsed.quantity"/></th>
                 <th class="tolerance"><spring:theme code="distributor.order.form.tolerance"/></th>
                <th class="pro-quantity"><spring:theme code="distributor.order.form.changed.proposed.quantity"/></th>
                 <th class="bu"><spring:theme code="distributor.order.form.bu"/></th>
                 <th class="hiearchy"><spring:theme code="distributor.order.form.product.hiearchy"/></th>
                    <th class="number"><spring:theme code="distributor.order.form.product.dependent.number"/></th>
                 <th class="crcy"><spring:theme code="distributor.order.form.crcy"/></th>
                 <th class="price"><spring:theme code="distributor.order.form.list.price"/></th>
                 <th class="open-quantity"><spring:theme code="distributor.order.form.open.quantity"/></th>
                 <th class="status"><spring:theme code="distributor.order.form.norm.status"/></th>
                 <th class="con-quantity"><spring:theme code="distributor.order.form.confirm.quantity"/></th>
                 <th class="total"><spring:theme code="distributor.order.form.total.price"/></th>
               
            </tr>
            <c:if test="${allItems ne null}">
            <c:forEach var="entries" items="${orderData.entries}">
                <c:set var="Slno" value="${Slno+1}" scope="page" />
                 <c:set var="orderData" value="${orderData}"/>
                <tr>
                	<!-- <td><c:out value="${Slno}"/></td> -->
                    <td class="product"><c:out value="${entries.product.code}" /></td>
                    <td class="material"><c:out value="${entries.product.name}" /></td>
                    <td class="norm"><fmt:parseNumber var="norm" integerOnly="true" 
                       type="number" value="${entries.distributorProductData.norm}" /><c:out value="${norm}" /></td>
                    <td class="sales"><fmt:parseNumber var="averageSales" integerOnly="true" 
                       type="number" value="${entries.distributorProductData.averageSales}" /><c:out value="${averageSales}" /></td>
                     <td class="transit"><fmt:parseNumber var="inTransit" integerOnly="true" 
                       type="number" value="${entries.distributorProductData.inTransit}" /><c:out value="${inTransit}" /></td>
                    <td class="days"><fmt:parseNumber var="coverDays" integerOnly="true" 
                       type="number" value="${entries.distributorProductData.coverDays}" /><c:out value="${coverDays}" /></td>
                    <td class="stock"><c:out value="${entries.distributorProductData.stock.stockLevel}" /></td>
                    <%-- <td class="quantity"><c:out value="${orderData.distributorProductData.proposedQty}" /></td> --%>
                    <td class="quantity">
                    	 <c:set var="proposedQnt" value="${entries.distributorProductData.norm-entries.distributorProductData.stock.stockLevel-entries.distributorProductData.inTransit-entries.distributorProductData.openQty}"/>
                   			 <c:choose>
                    			<c:when test="${proposedQnt < 0 }"><c:set var="proposedQnt" value="0"></c:set>${proposedQnt}</c:when>
                   					<c:otherwise><c:set var="proposedQnt" value="${proposedQnt}"></c:set><fmt:formatNumber value="${proposedQnt}" type="number" maxFractionDigits="0"/></c:otherwise>
                   			</c:choose>
                     </td>
                    <td><c:out value="+/- ${entries.distributorProductData.maxOrderQuantity} %" /></td>
                    <td><fmt:parseNumber var="changedProposedQty" integerOnly="true" 
                       type="number" value="${entries.distributorProductData.changedProposedQty}" /><c:out value="${changedProposedQty}" /></td>
                    
                     <c:set var="unitCode" value="${fn:toUpperCase(fn:substring(orderData.b2bCustomerData.unit.uid, 0, 3))}"></c:set>
                   <td class="bu">${unitCode}</td>
                    <td class="hiearchy">${categoryCode}</td>
                    <td class="number"></td>
                    <td></td>
                    
                    <td><c:out value="${entries.product.price.formattedValue}" /></td>
                    <td><fmt:parseNumber integerOnly="true" 
                       type="number" value="${entries.distributorProductData.openQty}"/></td>
                    <td></td>
                    <td><c:out value="${entries.quantity}" /></td>
                    <td><c:out value="${entries.totalPrice.value}" /></td>
                    
                    
                 
                </tr>
                
            </c:forEach>
            </c:if>
            </tbody>
        </table>
    </div>





