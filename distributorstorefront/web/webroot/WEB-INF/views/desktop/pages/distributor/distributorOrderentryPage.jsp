<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<style>
table, th, td {
    border: 1px solid black;
}
th,td { 
 width: 30%; 
 text-align:center;
}
 input {
 outline:none;
  
} 
.rectangle{
	width: 70px;height: 15px;border: darkred;
}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">

</script>



<form:form id="orderForm" method="post" action="${request.contextPath}/cart/order" modelAttribute="distributorOrderForm">
<div id="detail_tab">
<table style="width:100%">
	<tr> <td><h2>DETAILS</h2></td></tr>	
	<tr>
		<td style="width:auto;padding:0px;padding-top:8px;"><label for="View">View:</label><select id="View" name="View">
			 		<option value="standard view">[Standard View]</option>
				</select>
		</td>
	
		<td style="width:auto;padding:0px;"><button type="button"  class="buttonClass">Print Version</button></td>
		<td style="width:auto;padding:0px;"><button type="button"  class="buttonClass">Export</button></td>
		<td style="width:auto;padding:0px;"><button type="button"  class="buttonClass">Save</button></td>
		<td style="width:auto;padding:0px;"><button type="button"  class="buttonClass">TLB</button></td>
		<td style="width:auto;padding:0px;"><button type="button"  class="buttonClass">Refresh Page</button></td>
		<td style="width:auto;padding:0px;"><button type="button" class="buttonClass" style="float:left
margin-right:10px; !important">Refresh Price</button></td>
		
		<td style="width:auto;padding:0px;"><button type="button" class="buttonClass"  name="Submit"  onClick="checkOrderQuantity()">
						<spring:theme code="Submit" />
					</button></td>
		
		
	</tr>
</table>



<div align="center" >
        <table id="s">
        <tbody>
            <tr>
               <th style="width:100%" class="product"><spring:theme code="distributor.order.form.product.code"/></th>
                <th class="material"><spring:theme code="distributor.order.form.material.description"/></th>
                <th class="norm"><spring:theme code="distributor.order.form.norm"/></th>
                 <th class="sales"><spring:theme code="distributor.order.form.average.sales"/></th>
                 <th class="transit"><spring:theme code="distributor.order.form.in.transit"/></th>
                 <th class="days"><spring:theme code="distributor.order.form.cover.days"/></th>
                <th class="stock"><spring:theme code="distributor.order.form.stock"/></th>
                <th class="quantity"><spring:theme code="distributor.order.form.propsed.quantity"/></th>
                 <th class="tolerance"><spring:theme code="distributor.order.form.tolerance"/></th>
                <th class="pro-quantity"><spring:theme code="distributor.order.form.changed.proposed.quantity"/></th>
                 <th class="bu"><spring:theme code="distributor.order.form.bu"/></th>
                 <th><spring:theme code="distributor.order.form.product.hiearchy"/></th>
                 <th class="number"><spring:theme code="distributor.order.form.product.dependent.number"/></th>
                 <th class="crcy"><spring:theme code="distributor.order.form.crcy"/></th>
                 <th class="price"><spring:theme code="distributor.order.form.list.price"/></th>
                 <th class="open-quantity"><spring:theme code="distributor.order.form.open.quantity"/></th>
                 <th class="status"><spring:theme code="distributor.order.form.norm.status"/></th>
                 <th class="con-quantity"><spring:theme code="distributor.order.form.confirm.quantity"/></th>
                 <th class="total"><spring:theme code="distributor.order.form.total.price"/></th>
            </tr>
            <c:if test="${orderEntryData ne null}">
            <c:forEach var="entryData" items="${orderEntryData}" varStatus="status">
            
            
                <tr>
                    <td class="product"><input name="cartEntries[${status.index}].distributorProductData.code" value="${entryData.distributorProductData.code}" style="outline:none;" readonly="readonly"/></td>
                    <td class="material">${entryData.distributorProductData.name}</td>
                    <td class="norm"><fmt:parseNumber  integerOnly="true" 
                       type="number" value="${entryData.distributorProductData.norm}" /></td>
                    <td class="sales"><fmt:parseNumber  integerOnly="true" 
                       type="number" value="${entryData.distributorProductData.averageSales}" /></td>
                     <td class="transit"><fmt:parseNumber  integerOnly="true" 
                       type="number" value="${entryData.distributorProductData.inTransit}" /></td>
                    <td class="days"><fmt:parseNumber  integerOnly="true" 
                       type="number" value="${entryData.distributorProductData.coverDays}" /></td>
                    <td class="stock">${entryData.distributorProductData.stock.stockLevel}</td>
                    <td class="quantity">
                    	 <c:set var="proposedQnt" value="${entryData.distributorProductData.norm-entryData.distributorProductData.stock.stockLevel-entryData.distributorProductData.inTransit-entryData.distributorProductData.openQty}"/>
                   			 <c:choose>
                    			<c:when test="${proposedQnt < 0 }"><c:set var="proposedQnt" value="0"></c:set>${proposedQnt}</c:when>
                   					<c:otherwise><c:set var="proposedQnt" value="${proposedQnt}"></c:set><fmt:formatNumber value="${proposedQnt}" type="number" maxFractionDigits="0"/></c:otherwise>
                   			</c:choose>
                     </td>
                     
					 <td>+/- ${entryData.distributorProductData.maxOrderQuantity} %</td>
					 <%-- <c:set var="proposedQnt" value="${entryData.distributorProductData.proposedQty}"/> --%>
					 <c:set var="maxQnt" value="${entryData.distributorProductData.maxOrderQuantity}"/>
                    <td><fmt:parseNumber  integerOnly="true" 
                       type="number" value="${entryData.distributorProductData.changedProposedQty}" /></td>
                    <c:set var="unitCode" value="${fn:toUpperCase(fn:substring(unitCode, 0, 3))}"></c:set>
                   <td class="bu">${unitCode}</td>
					<td>${categoryCode}</td>
					<td class="number"></td>
					<td></td>
					
					<td><input class="orderEntry" type="hidden" id="pq_lp" value="<fmt:formatNumber value="${proposedQnt}" type="number" maxFractionDigits="0"/>_${entryData.distributorProductData.price.formattedValue}_${entryData.distributorProductData.code}"/>${entryData.distributorProductData.price.formattedValue}</td>
					<td><fmt:parseNumber  integerOnly="true" 
                       type="number" value="${entryData.distributorProductData.openQty}" /></td>
					<td></td>
					<td style="border: 1.9px medium;border-radius: 1px;">
					
						<input type= "text" class="rectangle"  id="${entryData.distributorProductData.code}_${status.index}" name="cartEntries[${status.index}].quantity" onchange="calculateValue('${entryData.distributorProductData.code}_${status.index}','${entryData.distributorProductData.price.formattedValue}','<fmt:formatNumber value="${proposedQnt}" type="number" maxFractionDigits="0"/>','${maxQnt}');"/>
					</td>
                    <td class="total" id="${entryData.distributorProductData.code}">Total</td>
                      
                </tr>
                 
            </c:forEach>
            </c:if>
            </tbody>
        </table>
    </div>
    <br/>
</div>
    </form:form>





