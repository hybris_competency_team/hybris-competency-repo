/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.sonata.distributor.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.commercefacades.product.converters.populator.CategoryUrlPopulator;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sonata.distributor.facades.DistributorFacade;
import com.sonata.distributor.facades.product.data.CategoryFeatureComponentData;


/**
 * Controller for home page
 */
@Controller
@Scope("tenant")
//@RequestMapping("/")
@RequestMapping(value = "/home")
public class HomePageController extends AbstractPageController
{
	@Resource(name = "commerceCategoryService")
	private CommerceCategoryService commerceCategoryService;

	@Resource(name = "categoryPopulator")
	private CategoryUrlPopulator categoryPopulator;

	@Resource(name = "defaultDistributorFacade")
	private DistributorFacade distributorFacade;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "cartService")
	private CartService cartService;

	@RequestMapping(method = RequestMethod.GET)
	public String home(@RequestParam(value = "logout", defaultValue = "false") final boolean logout, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (logout)
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.INFO_MESSAGES_HOLDER, "account.confirmation.signout.title");
			return REDIRECT_PREFIX + ROOT;
		}

		storeCmsPageInModel(model, getContentPageForLabelOrId(null));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(null));
		updatePageTitle(model, getContentPageForLabelOrId(null));

		return getViewForPage(model);
	}

	protected void updatePageTitle(final Model model, final AbstractPageModel cmsPage)
	{
		storeContentPageTitleInModel(model, getPageTitleResolver().resolveHomePageTitle(cmsPage.getTitle()));
	}


	@RequestMapping(value = "/getSubCategories/{categoryCode:.*}", method = RequestMethod.GET)
	public String subCategories(@PathVariable("categoryCode") final String categoryCode, final Model model)
			throws CMSItemNotFoundException
	{

		final List<CategoryFeatureComponentData> categoryFeatureComponentDataList = getDistributorFacade()
				.getCategoryFeatureComponent(categoryCode);

		model.addAttribute("categoryFeatureComponentList", categoryFeatureComponentDataList);

		storeCmsPageInModel(model, getContentPageForLabelOrId(null));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(null));
		updatePageTitle(model, getContentPageForLabelOrId(null));

		return getViewForPage(model);
	}

	//@RequireHardLogIn
	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public String getAllSubCategories(final Model model) throws CMSItemNotFoundException
	{

		final List<CategoryFeatureComponentData> categoryFeatureComponentDataList = getDistributorFacade()
				.getAllCatgoryFeatureComponents();

		model.addAttribute("categoryFeatureComponentList", categoryFeatureComponentDataList);

		storeCmsPageInModel(model, getContentPageForLabelOrId(null));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(null));
		updatePageTitle(model, getContentPageForLabelOrId(null));

		return getViewForPage(model);


		//return REDIRECT_PREFIX + "/home/getSubCategories/Brands";
	}

	public CommerceCategoryService getCommerceCategoryService()
	{
		return commerceCategoryService;
	}

	public void setCommerceCategoryService(final CommerceCategoryService commerceCategoryService)
	{
		this.commerceCategoryService = commerceCategoryService;
	}

	public CategoryUrlPopulator getCategoryPopulator()
	{
		return categoryPopulator;
	}

	public void setCategoryPopulator(final CategoryUrlPopulator categoryPopulator)
	{
		this.categoryPopulator = categoryPopulator;
	}

	public DistributorFacade getDistributorFacade()
	{
		return distributorFacade;
	}

	public void setDistributorFacade(final DistributorFacade distributorFacade)
	{
		this.distributorFacade = distributorFacade;
	}

	public UserService getUserService()
	{
		return userService;
	}

	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	/**
	 * @return the cartService
	 */
	public CartService getCartService()
	{
		return cartService;
	}

	/**
	 * @param cartService
	 *           the cartService to set
	 */
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}


}
