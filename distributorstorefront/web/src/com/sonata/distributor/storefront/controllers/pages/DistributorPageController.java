package com.sonata.distributor.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sonata.distributor.facades.DistributorFacade;
import com.sonata.distributor.storefront.forms.DistributorOrderForm;
//import de.hybris.platform.commercefacades.order.CartFacade;


//import com.sonata.distributor.facades.DistributorFacade;



@Controller
@Scope("tenant")
public class DistributorPageController extends AbstractPageController
{
	@Resource(name = "distributorFacade")
	DistributorFacade distributorFacade;

	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	@Resource(name = "userService")
	private UserService userService;

	public static final String REDIRECT_PREFIX = "redirect:";

	@RequestMapping(value = "/cart/{categoryCode}", method = RequestMethod.GET)
	public final String addProductToCart(@PathVariable("categoryCode") final String categoryCode, final Model model)
			throws CMSItemNotFoundException


	{
		getSessionService().setAttribute("categoryCode", categoryCode);
		userService.getCurrentUser().getUid();
		final Set<String> multidErrorMsgs = new HashSet<String>();


		final List<OrderEntryData> orderEntryData = distributorFacade.getProductDetails(categoryCode);

		final Map<String, String> distributorData = distributorFacade.getDistributorDetails();
		model.addAttribute("categoryCode", categoryCode);
		model.addAttribute("uid", distributorData.get("uid"));
		model.addAttribute("name", distributorData.get("name"));
		model.addAttribute("unitCode", distributorData.get("unitCode"));
		model.addAttribute("categoryCode", categoryCode);

		if (CollectionUtils.isNotEmpty(multidErrorMsgs))
		{
			model.addAttribute("multidErrorMsgs", multidErrorMsgs);
		}
		model.addAttribute("orderEntryData", orderEntryData);
		model.addAttribute("distributorData", distributorData);
		storeCmsPageInModel(model, getContentPageForLabelOrId("OrderentryPage"));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId("OrderentryPage"));


		return getViewForPage(model);

	}

	@RequestMapping(value = "/cart/order", method = RequestMethod.POST)
	public String orderConfirmation(@ModelAttribute("distributorOrderForm") final DistributorOrderForm form, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		final Set<String> multidErrorMsgs = new HashSet<String>();
		final List<CartModificationData> cartModificationDataList = new ArrayList<CartModificationData>();

		for (final OrderEntryData cartEntry : form.getCartEntries())
		{

			try
			{
				final long qty = cartEntry.getQuantity().longValue();
				final CartModificationData cartModificationData = cartFacade.addToCart(cartEntry.getDistributorProductData()
						.getCode(), qty);
				cartModificationDataList.add(cartModificationData);
			}
			catch (final CommerceCartModificationException ex)
			{
				multidErrorMsgs.add("basket.error.occurred");
			}



		}


		return REDIRECT_PREFIX + "/checkout/multi/express";



	}


}
