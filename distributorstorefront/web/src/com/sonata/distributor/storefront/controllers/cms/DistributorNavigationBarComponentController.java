/**
 *
 */
package com.sonata.distributor.storefront.controllers.cms;

import de.hybris.platform.acceleratorcms.enums.NavigationBarMenuLayout;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sonata.distributor.core.model.DistributorNavigationBarComponentModel;
import com.sonata.distributor.storefront.controllers.ControllerConstants;


/**
 * @author sahana.m
 *
 */

@Controller("DistributorNavigationBarComponentController")
@Scope("tenant")
@RequestMapping(value = ControllerConstants.Actions.Cms.DistributorNavigationBarComponent)
public class DistributorNavigationBarComponentController extends AbstractCMSComponentController<DistributorNavigationBarComponentModel>
{

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final DistributorNavigationBarComponentModel component)
	{
		if (component.getDropDownLayout() != null)
		{

			model.addAttribute("dropDownLayout", component.getDropDownLayout().getCode().toLowerCase());

		}
		else if (component.getNavigationNode() != null && component.getNavigationNode().getChildren() != null
				&& !component.getNavigationNode().getChildren().isEmpty())
		{
			model.addAttribute("dropDownLayout", NavigationBarMenuLayout.AUTO.getCode().toLowerCase());


		}
	}
}