/**
 *
 */
package com.sonata.distributor.storefront.controllers.cms;

import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sonata.distributor.core.model.DistributorNavigationBarCollectionComponentModel;
import com.sonata.distributor.storefront.controllers.ControllerConstants;


/**
 * @author sahana.m
 *
 */

@Controller("DistributorNavigationBarCollectionComponentController")
@Scope("tenant")
@RequestMapping(value = ControllerConstants.Actions.Cms.DistributorNavigationBarCollectionComponent)
public class DistributorNavigationBarCollectionComponentController extends
		AbstractCMSComponentController<DistributorNavigationBarCollectionComponentModel>
{
	@Resource(name = "userService")
	private UserService userService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final DistributorNavigationBarCollectionComponentModel component)
	{

		model.addAttribute("UserUid", userService.getCurrentUser().getUid());

	}


}