/**
 *
 */
package com.sonata.distributor.storefront.forms;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;

import java.util.List;



/**
 * @author sahana.m
 *
 */
public class DistributorOrderForm
{

	private List<OrderEntryData> cartEntries;

	/**
	 * @return Return the cartEntries.
	 */
	public List<OrderEntryData> getCartEntries()
	{
		return cartEntries;
	}

	/**
	 * @param cartEntries
	 *           The cartEntries to set.
	 */
	public void setCartEntries(final List<OrderEntryData> cartEntries)
	{
		this.cartEntries = cartEntries;
	}






}
